/*
 * Copyright (c) 2020 Adam Coldrick
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain
 * a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

import Axios from 'axios'

import base from './base.js'

const PROJECTS_URL = `${base.baseUrl}/projects`

export default {
  async get (projectId) {
    const { data: project } = await Axios.get(`${PROJECTS_URL}/${projectId}`)
    return project
  },

  async browse (params) {
    const query = base.buildQueryString(params)
    const { data: projects } = await Axios.get(`${PROJECTS_URL}?${query}`)
    return projects
  }
}
