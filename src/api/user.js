/*
 * Copyright (c) 2020 Adam Coldrick
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may
 * not use this file except in compliance with the License. You may obtain
 * a copy of the License at
 *
 *         http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations
 * under the License.
 */

import Axios from 'axios'
import SHA256 from 'crypto-js/sha256'

import base from './base.js'

const USERS_URL = `${base.baseUrl}/users`
const USER_CACHE = {}

export default {
  async get (userId) {
    const cacheKey = `user-${userId}`
    if (Object.hasOwnProperty.call(USER_CACHE, cacheKey)) {
      return USER_CACHE[cacheKey]
    }
    const { data: user } = await Axios.get(`${USERS_URL}/${userId}`)
    USER_CACHE[cacheKey] = user
    return user
  },

  getAvatarUrl (user, size) {
    if (!size) {
      size = 30
    }
    // TODO: Allow user to select email or openid for hashing
    const hash = SHA256(user.openid)
    return `${base.baseAvatarUrl}/${hash}?s=${size}&d=retro`
  },

  async browse (params) {
    const query = base.buildQueryString(params)
    const { data: users } = await Axios.get(`${USERS_URL}?${query}`)
    return users
  }
}
